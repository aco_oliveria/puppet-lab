#puppet module install puppetlabs-apache --version 12.0.3 --modulepath $puppetmodules
node 'puppetclient.ec2.internal' {
#  include apache

  class { 'apache':
    default_vhost => false,
  }

  apache::vhost { 'vhost.example.com':
    port    => 80,
    docroot => '/var/www/vhost',
  }

  file { '/var/www/vhost/index.html':
    ensure  => file,
    content => 'Teste aula puppet',
    require => Class['apache'],
  }

# Instalando docker
#  include 'docker'
  class { 'docker': }

  docker::run { 'nginx':
    image   => 'nginx:latest',
    ports   => ['8080:80'],
    volumes => ['/tmp/nginx1:/usr/share/nginx/html'],
    require => Class['docker'],
  }

  docker::run { 'nginx2':
    image   => 'nginx:latest',
    ports   => ['8081:80'],
    volumes => ['/tmp/nginx2:/usr/share/nginx/html'],
    require => Class['docker'],
  }
}