# puppet module install puppetlabs-apache --modulepath $puppetmodules
# puppet module install puppetlabs-docker --modulepath $puppetmodules
node 'puppetclient.ec2.internal' {
  # Incluindo o módulo do Apache
  class { 'apache':
    default_vhost => false,
  }

  # Configurando o virtual host
  apache::vhost { 'vhost.example.com':
    port    => 80,
    docroot => '/var/www/vhost',
  }

  # Criando o arquivo index.html personalizado para nginx1
   file { '/var/www/vhost/index1.html':
    ensure  => file,
    content => "<html><body><h1>Página 1</h1></body></html>",
    require => Class['apache'],

  }

  # Instalando o Docker
  class { 'docker': }
 

  # Criando o contêiner nginx1
   docker::run { 'nginx1':
     image   => 'nginx:latest',
     ports   => ['8080:80'],
     volumes => ['/var/www/vhost/index1.html:/usr/share/nginx/html/index.html'],
     require => File['/var/www/vhost/index1.html'],

 
  }

  # Criando o arquivo index.html personalizado para nginx2
  file { '/var/www/vhost/index2.html':
    ensure  => file,
    content => "<html><body><h1>Página 2</h1></body></html>",
    require => Class['apache'],
  }

  # Criando o contêiner nginx2
   docker::run { 'nginx2':
     image   => 'nginx:latest',
     ports   => ['8081:80'],
     volumes => ['/var/www/vhost/index2.html:/usr/share/nginx/html/index.html'],
     require => File['/var/www/vhost/index2.html'],
 }
}